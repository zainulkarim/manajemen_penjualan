<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang</title>
	<link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="public/custom/style.css">
</head>
<body class="index-page">
	<div class="container transparent">
		<h3 align="center">
			SELAMAT DATANG
		</h3>
		<hr class="hr">
		<h1 align="center" style="margin-top:200px;">
			SISTEM MANAJEMEN PENJUALAN
		</h1>
		<h4 align="center">
			Implementasi Kombinasi Metode K-Means dan KNN
		</h4>
		<div class="footer-welcome">
			<a href="login.php" class="btn btn-primary btn-block">Login</a>
		</div>
	</div>
</body>
</html>