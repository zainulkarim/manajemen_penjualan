<!DOCTYPE html>
<html>
<head>
	<title>Permission Denied!</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../custom/style.css">
</head>
<body>
	<h1 align="center" style="color:red;">Permission Denied</h1>
	<hr>
	<h4 align="center">You don't have any privilleges to visit this page!!!</h4>
	<a href="../../" class="btn btn-primary footer-welcome">Back To Home Page</a>
</body>
</html>