function active(elem){
	$('.active').attr('class','sub-menu')
	$(elem).attr('class','sub-menu active')
	localStorage.setItem('page', elem.id)
}
function active_id(elem){
	$('.active').attr('class','sub-menu')
	$('#'+elem).attr('class','sub-menu active')
	localStorage.setItem('page', elem.id)
}
$(document).ready(function(){
	elem = localStorage.getItem('page')
	$('.active').attr('class','sub-menu')
	$('#'+elem).attr('class','sub-menu active')
	localStorage.clear()
})