-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 13 Des 2018 pada 02.32
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manajemen_penjualan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `akses` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `akses`) VALUES
(1, 'Admin Sistem'),
(2, 'Admin Penjualan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `produk` int(100) NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `bulan` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `wilayah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`id`, `produk`, `jumlah`, `bulan`, `tahun`, `wilayah`) VALUES
(154, 4, '790', 1, 2018, 7),
(155, 4, '770', 2, 2018, 7),
(156, 4, '780', 3, 2018, 7),
(157, 4, '2147', 1, 2018, 8),
(158, 4, '2748', 2, 2018, 8),
(159, 4, '3045', 3, 2018, 8),
(160, 4, '2993', 1, 2018, 9),
(161, 4, '3017', 2, 2018, 9),
(162, 4, '2990', 3, 2018, 9),
(163, 4, '7687', 1, 2018, 10),
(164, 4, '7686', 2, 2018, 10),
(165, 4, '7687', 3, 2018, 10),
(166, 4, '34623', 1, 2018, 11),
(167, 4, '34708', 2, 2018, 11),
(168, 4, '34669', 3, 2018, 11),
(169, 4, '4023', 1, 2018, 12),
(170, 4, '4628', 2, 2018, 12),
(171, 4, '5229', 3, 2018, 12),
(172, 4, '7816', 1, 2018, 13),
(173, 4, '7421', 2, 2018, 13),
(174, 4, '7022', 3, 2018, 13),
(175, 4, '780', 1, 2018, 14),
(176, 4, '981', 2, 2018, 14),
(177, 4, '1179', 3, 2018, 14),
(178, 4, '6961', 1, 2018, 15),
(179, 4, '6902', 2, 2018, 15),
(180, 4, '7027', 3, 2018, 15),
(181, 4, '762', 1, 2018, 16),
(182, 4, '758', 2, 2018, 16),
(183, 4, '760', 3, 2018, 16),
(184, 5, '60', 1, 2018, 7),
(185, 5, '65', 2, 2018, 7),
(186, 5, '73', 3, 2018, 7),
(187, 5, '416', 1, 2018, 8),
(188, 5, '619', 2, 2018, 8),
(189, 5, '400', 3, 2018, 8),
(190, 5, '348', 1, 2018, 9),
(191, 5, '351', 2, 2018, 9),
(192, 5, '345', 3, 2018, 9),
(193, 5, '1466', 1, 2018, 10),
(194, 5, '1647', 2, 2018, 10),
(195, 5, '1259', 3, 2018, 10),
(196, 5, '2009', 1, 2018, 11),
(197, 5, '2007', 2, 2018, 11),
(198, 5, '2011', 3, 2018, 11),
(199, 5, '1461', 1, 2018, 12),
(200, 5, '1762', 2, 2018, 12),
(201, 5, '2060', 3, 2018, 12),
(202, 5, '1432', 1, 2018, 13),
(203, 5, '1305', 2, 2018, 13),
(204, 5, '1071', 3, 2018, 13),
(205, 5, '212', 1, 2018, 14),
(206, 5, '193', 2, 2018, 14),
(207, 5, '231', 3, 2018, 14),
(208, 5, '1369', 1, 2018, 15),
(209, 5, '1132', 2, 2018, 15),
(210, 5, '1553', 3, 2018, 15),
(211, 5, '325', 1, 2018, 16),
(212, 5, '425', 2, 2018, 16),
(213, 5, '430', 3, 2018, 16),
(214, 6, '89', 1, 2018, 7),
(215, 6, '99', 2, 2018, 7),
(216, 6, '111', 3, 2018, 7),
(217, 6, '472', 1, 2018, 8),
(218, 6, '475', 2, 2018, 8),
(219, 6, '476', 3, 2018, 8),
(220, 6, '378', 1, 2018, 9),
(221, 6, '380', 2, 2018, 9),
(222, 6, '383', 3, 2018, 9),
(223, 6, '1666', 1, 2018, 10),
(224, 6, '1970', 2, 2018, 10),
(225, 6, '1368', 3, 2018, 10),
(226, 6, '2092', 1, 2018, 11),
(227, 6, '2044', 2, 2018, 11),
(228, 6, '2140', 3, 2018, 11),
(229, 6, '2014', 1, 2018, 12),
(230, 6, '2215', 2, 2018, 12),
(231, 6, '2417', 3, 2018, 12),
(232, 6, '1397', 1, 2018, 13),
(233, 6, '1201', 2, 2018, 13),
(234, 6, '1002', 3, 2018, 13),
(235, 6, '106', 1, 2018, 14),
(236, 6, '197', 2, 2018, 14),
(237, 6, '286', 3, 2018, 14),
(238, 6, '1460', 1, 2018, 15),
(239, 6, '1057', 2, 2018, 15),
(240, 6, '1262', 3, 2018, 15),
(241, 6, '412', 1, 2018, 16),
(242, 6, '408', 2, 2018, 16),
(243, 6, '410', 3, 2018, 16),
(244, 7, '1525', 1, 2018, 7),
(245, 7, '1465', 2, 2018, 7),
(246, 7, '1410', 3, 2018, 7),
(247, 7, '4300', 1, 2018, 8),
(248, 7, '4917', 2, 2018, 8),
(249, 7, '4310', 3, 2018, 8),
(250, 7, '5861', 1, 2018, 9),
(251, 7, '5461', 2, 2018, 9),
(252, 7, '5072', 3, 2018, 9),
(253, 7, '9684', 1, 2018, 10),
(254, 7, '9851', 2, 2018, 10),
(255, 7, '9284', 3, 2018, 10),
(256, 7, '42109', 1, 2018, 11),
(257, 7, '44108', 2, 2018, 11),
(258, 7, '43111', 3, 2018, 11),
(259, 7, '17550', 1, 2018, 12),
(260, 7, '21853', 2, 2018, 12),
(261, 7, '20153', 3, 2018, 12),
(262, 7, '17269', 1, 2018, 13),
(263, 7, '16973', 2, 2018, 13),
(264, 7, '16074', 3, 2018, 13),
(265, 7, '2200', 1, 2018, 14),
(266, 7, '2501', 2, 2018, 14),
(267, 7, '2799', 3, 2018, 14),
(268, 7, '13459', 1, 2018, 15),
(269, 7, '13037', 2, 2018, 15),
(270, 7, '13281', 3, 2018, 15),
(271, 7, '2023', 1, 2018, 16),
(272, 7, '2101', 2, 2018, 16),
(273, 7, '2095', 3, 2018, 16),
(274, 8, '2913', 1, 2018, 7),
(275, 8, '3101', 2, 2018, 7),
(276, 8, '2126', 3, 2018, 7),
(277, 8, '4165', 1, 2018, 8),
(278, 8, '4366', 2, 2018, 8),
(279, 8, '4269', 3, 2018, 8),
(280, 8, '4920', 1, 2018, 9),
(281, 8, '4720', 2, 2018, 9),
(282, 8, '4710', 3, 2018, 9),
(283, 8, '19497', 1, 2018, 10),
(284, 8, '18601', 2, 2018, 10),
(285, 8, '17402', 3, 2018, 10),
(286, 8, '24167', 1, 2018, 11),
(287, 8, '25165', 2, 2018, 11),
(288, 8, '24069', 3, 2018, 11),
(289, 8, '14452', 1, 2018, 12),
(290, 8, '14887', 2, 2018, 12),
(291, 8, '14641', 3, 2018, 12),
(292, 8, '14586', 1, 2018, 13),
(293, 8, '13852', 2, 2018, 13),
(294, 8, '12331', 3, 2018, 13),
(295, 8, '3202', 1, 2018, 14),
(296, 8, '3628', 2, 2018, 14),
(297, 8, '4350', 3, 2018, 14),
(298, 8, '11375', 1, 2018, 15),
(299, 8, '9372', 2, 2018, 15),
(300, 8, '10377', 3, 2018, 15),
(301, 8, '3007', 1, 2018, 16),
(302, 8, '3308', 2, 2018, 16),
(303, 8, '3305', 3, 2018, 16);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `produk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `produk`) VALUES
(7, 'Bengbeng'),
(4, 'Better'),
(8, 'Choky-choky'),
(5, 'Kis Barley'),
(6, 'Kis Cherry');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `akses_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `akses_id`) VALUES
(1, 'zainul', 'b4b6bd87bee8888e82d22327fbb9cf70', 'zainul karim', 1),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wilayah`
--

CREATE TABLE `wilayah` (
  `id` int(11) NOT NULL,
  `wilayah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `wilayah`
--

INSERT INTO `wilayah` (`id`, `wilayah`) VALUES
(7, 'Badas'),
(8, 'Banyakan'),
(9, 'Gampengrejo'),
(10, 'Grogol'),
(11, 'Gurah'),
(12, 'Kandangan'),
(13, 'Kandat'),
(14, 'Kayen Kidul'),
(15, 'Kepung'),
(16, 'Kunjang');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan_produk` (`produk`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produk` (`produk`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `akses_id` (`akses_id`);

--
-- Indeks untuk tabel `wilayah`
--
ALTER TABLE `wilayah`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wilayah` (`wilayah`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=304;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `wilayah`
--
ALTER TABLE `wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`akses_id`) REFERENCES `akses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
