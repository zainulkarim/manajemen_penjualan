<?php
	include('../config/settings.php');

	function totalPenjualanWilayahPerBulan($wilayahId, $bulan){
		include('../config/settings.php');
		$totalq = mysqli_query($connection, 'SELECT sum(jumlah) as total FROM `penjualan` where bulan='.$bulan.' and wilayah='.$wilayahId);
		$total = mysqli_fetch_row($totalq);
		return $total[0];		
	}

	function totalPenjualanWilayahPerProduk($wilayah, $produk){
		include('../config/settings.php');
		$wilayahQuery = mysqli_query($connection, 'SELECT id FROM `wilayah` where wilayah="'.$wilayah.'"');
		$wilayahId = mysqli_fetch_row($wilayahQuery)[0];
		$totalq = mysqli_query($connection, 'SELECT sum(jumlah) as total FROM `penjualan` where wilayah='.$wilayahId.' and produk='.$produk);
		$total = mysqli_fetch_row($totalq);
		return $total[0];
	}

	function getBobotByProdukId($produkId){
		include('../config/settings.php');
		$bobotq = mysqli_query($connection, 'SELECT jumlah FROM `bobot` where produk='.$produkId);
		$bobot = mysqli_fetch_row($bobotq);
		return $bobot[0];
	}

	function getEuclideanDistance($a, $b){
		return sqrt(pow($a-$b, 2));
	}

	function rerataCluster($arr, $defaultArr){
		$totalArr = count($arr);
		if($totalArr == 1){
			return $defaultArr;
		}
		else{
			$s1 = 0;
			$s3 = 0;
			$s2 = 0;
			foreach ($arr as $key => $value) {
				foreach($value as $subKey => $subValue){
					if($subKey == 0){
						$s1+=$subValue;
					}
					else if($subKey == 1){
						$s2+=$subValue;
					}
					else if($subKey == 2){
						$s3+=$subValue;
					}
				}
			}
			return [($s1/$totalArr), ($s2/$totalArr), ($s3/$totalArr)];
		}
	}

?>
<table class="table table-hover table-stripped">
<!-- <table width="100%"> -->
	<tr>
		<th>No</th>
		<th>Nama produk</th>
		<?php
			$bulan = mysqli_query($connection, 'SELECT * FROM `penjualan` GROUP BY bulan');
			$i = 1;
			while($row=mysqli_fetch_array($bulan)){
			?>
				<th>
					<?php
						if ($row['bulan'] == 1){
							echo "JAN-APR";
						}
						else if ($row['bulan'] == 2){
							echo "MEI-AGU";
						}
						else if ($row['bulan'] == 3){
							echo "SEP-DES";
						}
						else{
							echo "";
						}
					?>
				</th>
			<?php
			$i++;
			}
		?>
		<th>Total</th>
	</tr>
	<?php
	$q_produk = mysqli_query($connection, 'SELECT * FROM `penjualan` GROUP BY wilayah');
	$i = 1;

	$totalPenjualan=[];

	while($row=mysqli_fetch_array($q_produk)){
	$wilayah_id = $row['wilayah'];
	?>
	<tr>
		<td><?php echo $i;?></td>
		<td>
			<?php
				$wilayah = mysqli_query($connection, 'select * from wilayah where id='.$wilayah_id);
				while($wil=mysqli_fetch_array($wilayah)){
					echo $wil['wilayah'];
				}
			?>
		</td>
		<?php
		$bulan = mysqli_query($connection, 'SELECT * FROM `penjualan` GROUP BY bulan');
			$j = 1;
			while($bln=mysqli_fetch_array($bulan)){
			?>
				<td>
					<?php
						echo totalPenjualanWilayahPerBulan($wilayah_id, $bln['bulan']);
						$totalPenjualan[$wilayah_id][$bln['bulan']] = totalPenjualanWilayahPerBulan($wilayah_id, $bln['bulan']);
					?>
				</td>
			<?php
			$j++;
			}
		?>
		<td>
			<?php
				$totalq = mysqli_query($connection, 'SELECT sum(jumlah) as total FROM `penjualan` where wilayah='.$wilayah_id);
				$total = mysqli_fetch_row($totalq);
				echo $total[0];
			?>
		</td>
	</tr>
	<?php
	$i++;
	}
	?>
</table>

<h4>K-Means Clustering</h4>
<hr/>
<h5>Bobot Awal</h5>
<table class="table table-hover table-stripped">
<!-- <table width="100%"> -->
	<tr>
		<th>No</th>
		<th>Nama Cluster</th>
		<?php
			$bulan = mysqli_query($connection, 'SELECT * FROM `penjualan` GROUP BY bulan');
			$i = 1;
			while($row=mysqli_fetch_array($bulan)){
			?>
				<th>
					<?php
						if ($row['bulan'] == 1){
							echo "JAN-APR";
						}
						else if ($row['bulan'] == 2){
							echo "MEI-AGU";
						}
						else if ($row['bulan'] == 3){
							echo "SEP-DES";
						}
						else{
							echo "";
						}
					?>
				</th>
			<?php
			$i++;
			}
		?>
	</tr>
	<?php
	$cluster = [];
	$cluster_q = mysqli_query($connection, 'SELECT * FROM `cluster`');
	$i = 1;
	while($row=mysqli_fetch_array($cluster_q)){
		$c[$row['nama']] = [$row['segmen_satu'], $row['segmen_dua'], $row['segmen_tiga']];
	?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['segmen_satu'];?></td>
		<td><?php echo $row['segmen_dua'];?></td>
		<td><?php echo $row['segmen_tiga'];?></td>
	</tr>
	<?php
	$i++;
	}
	?>
</table>



<h5>Inisiasi 1</h5>
<table class="table table-hover table-stripped">
	<tr>
		<th>No</th>
		<th>Wilayah</th>
		<th>C1</th>
		<th>C2</th>
		<th>C3</th>
	</tr>
	<?php
		$wilayah = mysqli_query($connection, 'SELECT * FROM `wilayah`');
		$i = 1;
		while($row=mysqli_fetch_array($wilayah))
		{
			$c1 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C1'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C1'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C1'][2]), 2));

			$c2 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C2'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C2'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C2'][2]), 2));

			$c3 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C3'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C3'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C3'][2]), 2));

			$clusterN[$row['wilayah']] = [$c1, $c2, $c3];
			$dataPenjualan[$row['wilayah']] = [$totalPenjualan[$row['id']][1], $totalPenjualan[$row['id']][2],$totalPenjualan[$row['id']][3]]
	?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $row['wilayah'];?></td>
				<td><?php echo $c1;?></td>
				<td><?php echo $c2;?></td>
				<td><?php echo $c3;?></td>
			</tr>
	<?php
			$i++;
		}
	?>
</table>

<!-- <h5>Cluster 1</h5> -->
<table class="table table-hover table-stripped">
	<tr>
		<th>No</th>
		<th>Wilayah</th>
		<th>Cluster</th>
	</tr>
	<?php
		$wilayah = mysqli_query($connection, 'SELECT * FROM `wilayah`');
		$i = 1;
		while($row=mysqli_fetch_array($wilayah))
		{

			$min = min($clusterN[$row['wilayah']]);
			foreach ($clusterN[$row['wilayah']] as $key => $value) {
				if ($min == $value){
					$segment = "C".($key+1);
					$segmentData[$row['wilayah']] = $segment;
				}
			}
	?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $row['wilayah'];?></td>
				<td><?php echo $segment;?></td>
			</tr>
	<?php
			$i++;
		}
	?>
</table>


<!-- looping cluster & iniasiasi -->
<?php
	$counts = array_count_values($segmentData);
	$end = 0;
	$counter = 2;
	while ($end != 1) {
	
		// menentukan $c baru untuk cluster selanjutnya
	$dataHasilCluster['C1'] = [];
	$dataHasilCluster['C2'] = [];
	$dataHasilCluster['C3'] = [];
	foreach ($segmentData as $key => $value) {
		array_push($dataHasilCluster[$value],$dataPenjualan[$key]);
	}

	$c1 = rerataCluster($dataHasilCluster['C1'], $c['C1']);
	$c2 = rerataCluster($dataHasilCluster['C2'], $c['C2']);
	$c3 = rerataCluster($dataHasilCluster['C3'], $c['C3']);
	$x = [
		'C1' => $c1,
		'C2' => $c2,
		'C3' => $c3,
	];
	// print_r($x);
	$c=$x;

		?>

		<h5>Cluster <?php echo($counter-1) ?></h5>

		<table class="table table-hover table-stripped">
		<!-- <table width="100%"> -->
			<tr>
				<th>No</th>
				<th>Nama Cluster</th>
				<?php
					$bulan = mysqli_query($connection, 'SELECT * FROM `penjualan` GROUP BY bulan');
					$i = 1;
					while($row=mysqli_fetch_array($bulan)){
					?>
						<th>
							<?php
								if ($row['bulan'] == 1){
									echo "JAN-APR";
								}
								else if ($row['bulan'] == 2){
									echo "MEI-AGU";
								}
								else if ($row['bulan'] == 3){
									echo "SEP-DES";
								}
								else{
									echo "";
								}
							?>
						</th>
					<?php
					$i++;
					}
				?>
			</tr>
			<?php

			$index = 1;

																						// '''
																						// awal mula permasalahan perbedaan jumlah hasil cluster

																						// '''
			$data = [];
			foreach($c as $key=>$value){
				echo "<tr>";
				echo "<td>".$index."</td>";
				echo "<td>".$key."</td>";
				foreach($value as $subkey=>$subvalue){
					echo "<td>".$subvalue."</td>";
					$data[] = $subvalue;
				}
				echo "</tr>";
				$index++;
			}
			?>
		</table>

		<br/><br/>
		<h5>Inisiasi <?php echo $counter; ?></h5>
		<table class="table table-hover table-stripped">
			<tr>
				<th>No</th>
				<th>Wilayah</th>
				<th>C1</th>
				<th>C2</th>
				<th>C3</th>
			</tr>
			<?php
				$wilayah = mysqli_query($connection, 'SELECT * FROM `wilayah`');
				$i = 1;
				while($row=mysqli_fetch_array($wilayah))
				{
					$c1 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C1'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C1'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C1'][2]), 2));

					$c2 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C2'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C2'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C2'][2]), 2));

					$c3 = sqrt(pow(($totalPenjualan[$row['id']][1] - $c['C3'][0]), 2) + pow(($totalPenjualan[$row['id']][2] - $c['C3'][1]), 2) + pow(($totalPenjualan[$row['id']][3] - $c['C3'][2]), 2));

					$clusterN2[$row['wilayah']] = [$c1, $c2, $c3];
			?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $row['wilayah'];?></td>
						<td><?php echo $c1;?></td>
						<td><?php echo $c2;?></td>
						<td><?php echo $c3;?></td>
					</tr>
			<?php
					$i++;
				}
			?>
		</table>

		<!-- <h5>Cluster <?php echo $counter; ?></h5> -->
		<table class="table table-hover table-stripped">
			<tr>
				<th>No</th>
				<th>Wilayah</th>
				<th>Cluster</th>
			</tr>
			<?php
				$wilayah = mysqli_query($connection, 'SELECT * FROM `wilayah`');
				$i = 1;
				while($row=mysqli_fetch_array($wilayah))
				{

					$min = min($clusterN2[$row['wilayah']]);
					foreach ($clusterN2[$row['wilayah']] as $key => $value) {
						if ($min == $value){
							$segment = "C".($key+1);
							$segmentData2[$row['wilayah']] = $segment;
						}
					}
			?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $row['wilayah'];?></td>
						<td><?php echo $segment;?></td>
					</tr>
			<?php
					$i++;
				}
			?>
		</table>
		<?php

		if($segmentData == $segmentData2){
			$end = 1;
		}
		else{
			$segmentData = $segmentData2;
		}
		$counter++;
		if($counter == 20){
			$end = 1;
		}
	}
?>
<br><br>
<h4>Hasil Perhitungan Jumlah Perjualan per Periode dan Wilayah</h4>
<h5>Penjualan Rendah</h5>
<table class="table table-hover table-stripped">
	<tr>
		<th>Nomor</th>
		<th>Wilayah</th>
		<?php
		$wilayah = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
		while($row=mysqli_fetch_array($wilayah))
		{
			echo "<th>".$row['produk']."</th>";
		}
		?>

	</tr>
	<?php
		$counter = 1;
		$penjualanTerendah = [];
		foreach ($segmentData as $key => $value) {
			if ($value == 'C3') {
				echo "<tr><td>".$counter."</td><td>".$key."</td>";
				$produk = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
				$i=0;
				while($row=mysqli_fetch_array($produk))
				{
					echo "<td>".totalPenjualanWilayahPerProduk($key, $row['id'])."</td>";
					$penjualanTerendah[$key][$i]=totalPenjualanWilayahPerProduk($key, $row['id']);
					$i++;
				}

				$counter+=1;
				echo "</tr>";
			}
		}
	?>
</table>
<h4>Bobot Terendah untuk menentukan penjualan</h4>
<table class="table table-hover table-stripped">
	<tr>
		<th>Nomor</th>
		<th>Produk</th>
		<th>Bobot</th>

	</tr>
	<?php
		$bobot = [];
		$counter = 1;
		$produk = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
		$i=1;
		while($row=mysqli_fetch_array($produk))
		{
			echo "<tr>";
			echo "<td>".$i."</td>";
			echo "<td>".$row['produk']."</td>";
			$bobot[$row['produk']] = getBobotByProdukId($row['id']);
			echo "<td>".getBobotByProdukId($row['id'])."</td>";
			echo "</tr>";
			$i++;
		}

	?>
</table>

<h4>Menentukan jarak penjualan produk terendah</h4>

<table class="table table-hover table-stripped">
	<tr>
		<th>Nomor</th>
		<th>Wilayah</th>
		<?php
		$wilayah = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
		while($row=mysqli_fetch_array($wilayah))
		{
			echo "<th>".$row['produk']."</th>";
		}
		?>

	</tr>
	<?php
		$counter = 1;
		$knnValue = [];
		foreach ($segmentData as $key => $value) {
			if ($value == 'C3') {
				echo "<tr><td>".$counter."</td><td>".$key."</td>";
				$produk = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
				$i=0;
				while($row=mysqli_fetch_array($produk))
				{
					echo "<td>".getEuclideanDistance($penjualanTerendah[$key][$i], $bobot[$row['produk']])."</td>";
					$knnValue[$row['produk']][$key] = getEuclideanDistance($penjualanTerendah[$key][$i], $bobot[$row['produk']]);
					$i++;
				}
				

				$counter+=1;
				echo "</tr>";
			}
		}
	?>
</table>

<h4>Dari perhitungan menggunakan metode KNN didapatkan hasil sebagai berikut :</h4>

<table class="table table-hover table-stripped">
	<tr>
		<?php
		$wilayah = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
		while($row=mysqli_fetch_array($wilayah))
		{
			echo "<th>".$row['produk']."</th>";
		}
		?>

	</tr>
	<?php
		$counter = 1;
		echo "<tr>";
		$produk = mysqli_query($connection, 'SELECT * FROM `produk` order by id asc');
		$min = 0;
		while($row=mysqli_fetch_array($produk))
		{
			$min = min($knnValue[$row['produk']]);
			foreach ($knnValue[$row['produk']] as $key => $value) {
				if($value == $min){
					echo "<td>".$key."</td>";
				}
			}
		}
		// echo "<td>".min($knnValue[])."</td>";
		echo "</tr>";
	?>
</table>