<?php
session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
	// $base_dir = $_SESSION['base_dir'];
	// header("Location: /manajemen_penjualan/sistem/index.php?page=beranda");
?>

<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
	<script type="text/javascript" src="../public/js/jquery.js"></script>
	<script type="text/javascript" src="../public/custom/script.js"></script>
</head>
<body class="admin-page-body">
	<div class="static-left" style="z-index: 1000000;">
		<div class="admin-title-page">
			<span>
				ADMIN PAGE
			</span>
		</div>
		<div class="admin-profile">
			<img src="../public/img/user.png" width="150px">
			<div class="admin-username">
				<?php
					echo $_SESSION['nama'];
				?>
			</div>
			<div class="admin-username">
				<?php
					echo $_SESSION['akses'];
				?>
			</div>
		</div>
		<div class="menu">
			<a href="?page=beranda">
				<div class="sub-menu active" onclick="active(this)" id="beranda">
					Beranda	
				</div>
			</a>
			<a href="?page=produk">
				<div class="sub-menu" onclick="active(this)"id="produk">
					Produk
				</div>
			</a>
			<a href="?page=wilayah">
				<div class="sub-menu" onclick="active(this)" id="wilayah">
					Wilayah
				</div>
			</a>
			<a href="?page=pendapatan">
				<div class="sub-menu" onclick="active(this)" id="pendapatan">
					Pendapatan
				</div>
			</a>
			<a href="?page=perhitungan">
				<div class="sub-menu" onclick="active(this)" id="perhitungan">
					Perhitungan
				</div>
			</a>
		</div>
	</div>
	<div class="admin-content">
		<?php
		if(isset($_GET['page'])){
			$page= $_GET['page'];
		}
		else{
			$page = "Beranda";
		}
		$action = "";
		if(isset($_GET['action'])){
			$action = $_GET['action'];
		}
		?>
		<h3 style="text-transform:capitalize;"><?php echo $action." ".$page;?></h3>
		<hr class="admin-hr">
		<div class="content-wrapper">
			
		<?php
		if(isset($page)){
			if($page == 'beranda'){
				include('beranda.php');
			}
			else if($page == 'produk'){
				if($action == 'edit'){
					include('edit_produk.php');
				}
				else if($action == 'delete'){
					include('delete_produk.php');
				}
				else{
					include('produk.php');
				}
			}
			else if($page == 'wilayah'){
				if($action == 'edit'){
					include('edit_wilayah.php');
				}
				else if($action == 'delete'){
					include('delete_wilayah.php');
				}
				else{
					include('wilayah.php');
				}
			}
			else if($page == 'pendapatan'){
				if($action == 'edit'){
					include('edit_pendapatan.php');
				}
				else if($action == 'delete'){
					include('delete_pendapatan.php');
				}
				else{
					include('pendapatan.php');
				}
			}
			else if($page == 'perhitungan'){
				include('perhitungan.php');
			}
			else{
				include('beranda.php');
			}
		}
		else{
			echo "Selamat Datang Di Sistem ini";
		}
		?>
		</div>
	</div>
</body>
</html>
<?php } ?>