<h3>Ubah Data pendapatan</h3>
<form action="?page=pendapatan" method="POST">
<table class="table">
	<?php
		include('../config/settings.php');
		$q_data = mysqli_query($connection, 'select * from penjualan where id='.$_GET['id']);
		while($row=mysqli_fetch_array($q_data)){
			?>
			<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
			<input type="hidden" name="update" value="update">

			<tr>
				<td width="30%">Nama produk</td>
				<td>:</td>
				<td width="69%">
					<select name="produk" class="form-control">
						<option value="">---</option>
						<?php
							$q_produk = mysqli_query($connection, 'select * from produk');
							$i = 1;
							while($r=mysqli_fetch_array($q_produk)){
								$selected = ($r['id'] == $row['produk'])?'selected':'';
								echo "<option value='".$r['id']."'".$selected.">".$r['produk']."</option>";
								$i++;
							}
						?>		
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">Bulan</td>
				<td>:</td>
				<td width="69%">
					<select name="bulan" class="form-control">
						<option value="1" <?php if($row['produk'] == 1) echo "selected"; ?> >Jan-Apr</option>
						<option value="2" <?php if($row['produk'] == 2) echo "selected"; ?> >Mei-Agu</option>
						<option value="3" <?php if($row['produk'] == 3) echo "selected"; ?> >Sep-Des</option>
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">Tahun</td>
				<td>:</td>
				<td width="69%">
					<select name="tahun" class="form-control">
						<?php
							for ($i=2018; $i < 2025; $i++) { 
								echo"<option value='$i'>$i</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">Wilayah</td>
				<td>:</td>
				<td width="69%">
					<select name="wilayah" class="form-control">
						<option value="">---</option>
						<?php
							$q_wilayah = mysqli_query($connection, 'select * from wilayah');
							$i = 1;
							while($r=mysqli_fetch_array($q_wilayah)){
								$selected = ($r['id'] == $row['wilayah'])?'selected':'';
								echo "<option value='".$r['id']."'".$selected.">".$r['wilayah']."</option>";
								$i++;
							}
						?>		
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">Jumlah</td>
				<td>:</td>
				<td width="69%"><input type="text" name="jumlah" class="form-control" maxlength="150" value="<?php echo $row['jumlah'];?>"></td>
			</tr>

			<?php
		}
	?>
	<tr>
	<td colspan="3">
		<input type="submit" class="btn btn-success" style="float:right; margin:2px;" value="Ubah">
		<a href="?page=pendapatan" class="btn btn-success" style="float:right; margin:2px;">Kembali</a>
	</td>
	</tr>
</table>
</form>