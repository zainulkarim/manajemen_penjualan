<?php
include('../config/settings.php');
	if(isset($_POST['insert'])){
		$produk =  $_POST['produk'];
		$bulan =  $_POST['bulan'];
		$tahun =  $_POST['tahun'];
		$jumlah =  $_POST['jumlah'];
		$wilayah =  $_POST['wilayah'];

		$simpan = mysqli_query($connection, "INSERT INTO penjualan (`id`,`bulan`,`tahun`, `produk`, `wilayah`, `jumlah`) VALUES ('', '$bulan', '$tahun', '$produk', '$wilayah', '$jumlah')");
		if (!$simpan) {
			die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal tersimpan <br>Kode Error : " . mysqli_error()."</div>");
		}
		else{
			($simpan);
			echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data sudah tersimpan </div>";
		}
	}
	else if(isset($_POST['update'])){
		$id =  $_POST['id'];
		$produk =  $_POST['produk'];
		$jumlah =  $_POST['jumlah'];
		$bulan =  $_POST['bulan'];
		$tahun =  $_POST['tahun'];
		$wilayah =  $_POST['wilayah'];


		$update = mysqli_query($connection, "UPDATE penjualan SET produk='$produk', bulan='$bulan', tahun='$tahun', wilayah='$wilayah', jumlah='$jumlah' WHERE id='$id' ");
		if (!$update) {
			die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal diubah <br>Kode Error : " . mysqli_error()."</div>");
		}
		else{
			($update);
			echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil diubah </div>";
		}
	}
	else if(isset($_POST['delete'])){
		$id =  $_POST['id'];
		$delete = mysqli_query($connection, "DELETE from penjualan WHERE id='$id' ");
		if (!$delete) {
			die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal dihapus <br>Kode Error : " . mysqli_error()."</div>");
		}
		else{
			($delete);
			echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil dihapus </div>";
		}
	}
?>
			<h3>Data Penjualan</h3>
			<form action="index.php?page=pendapatan" method="POST">
			<input type="hidden" name="insert" value="insert">
			<table class="table">
			<tr>
				<td colspan="3">
					<table width="100%">
						<tr>
							<td width="15%">Bulan</td>
							<td width="20%">Tahun</td>
							<td width="20%">Wilayah</td>
							<td width="20%">Produk</td>
							<td width="15%">Jumlah</td>
						</tr>
						<tr>
							<td>
								<select name="bulan" class="form-control">
									<option value="1">Jan-Apr</option>
									<option value="2">Mei-Agu</option>
									<option value="3">Sep-Des</option>
								</select>
							</td>
							<td>
								<select name="tahun" class="form-control">
									<?php
										for ($i=2018; $i < 2025; $i++) { 
											echo"<option value='$i'>$i</option>";
										}
									?>
								</select>
							</td>
							<td>
								<select name="wilayah" class="form-control">
									<option value="">---</option>
									<?php
										$q_wilayah = mysqli_query($connection, 'select * from wilayah');
										$i = 1;
										while($row=mysqli_fetch_array($q_wilayah)){
										?>
										<option value='<?php echo $row['id']?>'><?php echo $row['wilayah']?></option>
										<?php
											$i++;
										}
									?>		
								</select>
							</td>
							<td>
								<select name="produk" class="form-control">
									<option value="">---</option>
									<?php
										$q_wilayah = mysqli_query($connection, 'select * from produk');
										$i = 1;
										while($row=mysqli_fetch_array($q_wilayah)){
										?>
										<option value='<?php echo $row['id']?>'><?php echo $row['produk']?></option>
										<?php
											$i++;
										}
									?>		
								</select>
							</td>
							<td>
								<input type="number" name="jumlah" class="form-control">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="submit" class="btn btn-success" style="float:right;" value="Simpan">
				</td>
			</tr>
			</table>
			</form>

			<table class="table table-hover table-stripped">
			<!-- <table width="100%"> -->
				<tr>
					<th>No</th>
					<th>Nama produk</th>
					<th>Bulan</th>
					<th>Tahun</th>
					<th>Wilayah</th>
					<th>Jumlah</th>
				</tr>
				<?php
				$q_produk = mysqli_query($connection, 'SELECT * FROM `penjualan` ORDER BY `penjualan`.`bulan` ASC');
				$i = 1;
				while($row=mysqli_fetch_array($q_produk)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td>
						<?php
							$produk_id = $row['produk'];
							$produk = mysqli_query($connection, 'select * from produk where id='.$produk_id);
							while($prod=mysqli_fetch_array($produk)){
								echo $prod['produk'];
							}
						?>
					</td>
					<td>
						<?php
							if ($row['bulan'] == 1){
								echo "JAN-APR";
							}
							else if ($row['bulan'] == 2){
								echo "MEI-AGU";
							}
							else if ($row['bulan'] == 3){
								echo "SEP-DES";
							}
							else{
								echo "";
							}
						?>	
					</td>
					<td><?php echo $row['tahun']?></td>
					<td>
						<?php
							$wilayah_id = $row['wilayah'];
							$wilayah = mysqli_query($connection, 'select * from wilayah where id='.$wilayah_id);
							while($wil=mysqli_fetch_array($wilayah)){
								echo $wil['wilayah'];
							}
						?>
					</td>
					<td><?php echo $row['jumlah']?></td>
					<td>
						<a class="btn btn-info" href="<?php echo "?page=pendapatan&action=edit&id=".$row['id']; ?>">Edit</a>
						<a class="btn btn-danger" href="<?php echo "?page=pendapatan&action=delete&id=".$row['id']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
				$i++;
				}
				?>
			</table>